import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IndexCalcTest {
	private IndexCalc calculator;
	@BeforeEach
	public void Setup() throws Exception{
		calculator=new IndexCalc();
	}
		@Test
	public void testCmConvertingIntoMeterWell() {
		double  result=calculator.HeightToMeter(200);
		assertEquals(2.00,result);
	}
		@Test
		public void testInchConvertingIntoMeterWell() {
			double result=calculator.HeightToMeter(50);
			assertEquals(1.27,result);
		}
	@Test
	public void testInterpretationWithExtremeNumbers() {
		String result=calculator.Interpretation(1);
		assertEquals("Wrong Data",result);
	}
	@Test
	public void testInterpretationWithNormal() {
		String result=calculator.Interpretation(20);
		assertEquals("Normal (20.0 kg/m2)",result);
	}
	@Test
	public void testCalculationWithValidNumbers() {
		String result=calculator.CalculatingIndex(2, 100);
		assertEquals("Overweight (25.0 kg/m2)",result);
	}
	@Test
	public void testCalculationWithWrongNumbers() {
		String result=calculator.CalculatingIndex(2530, 500);
		assertEquals("False Data",result);
	}
	@Test
	public void testCalculationWithWrongHeight() {
		String result=calculator.CalculatingIndex(2530, 10);
		assertEquals("False Data",result);
	}
	@Test
	public void testCalculationWithWrongWeight() {
		String result=calculator.CalculatingIndex(2, 500);
		assertEquals("False Data",result);
	}

}
