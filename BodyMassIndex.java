/**
 * BodyMassIndex Interface
 * 
 * @author Kabai Csaba
 *
 */
public interface BodyMassIndex {
	/**
	 * 
	 * @param height
	 * @param weight
	 * @return with the index and the category
	 */
	String CalculatingIndex(double height, double weight);
}
