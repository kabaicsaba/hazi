
/**
 * IndexCalc class
 * 
 * @author Kabai Csaba
 *
 */
public class IndexCalc implements BodyMassIndex {

	private double index; //Body Mass Index in number
	private String result; //Body Mass Index Category
	/**
	 * Converts the height component from cm/inches into m.
	 * @param height
	 * @return height in m.
	 */
	protected Double HeightToMeter(double height) {
		//if it is in cm
		if((height>=100)&&(height<250)) {
			height=height/100;
		//if it is in inch
		}else if((height<100)&&(height>=3)) {
			height=(height*2.54)/100;
		}
		return height;
	}

	@Override
	public String CalculatingIndex(double height, double weight) {
		//Avoiding false numbers
		if((height>0)&&(weight>0)&&(height<250)&&(weight<250)) {
			height=HeightToMeter(height);
			//Body Mass Index Formula
			index=weight/Math.pow(height, 2);
			//Converting into 2 decimals
			index=index*100;
			index=Math.round(index);
			index=index/100;
			
			result=Interpretation(index);
			return result;
		}else {
			return "False Data";
		}
		
	}
/**
 * BMI Categories,
 * Source: http://www.calculator.net/bmi-calculator.html
 * @param index
 * @return
 */
	protected String Interpretation(double index) {
		//at extreme results maybe the units are wrong
		if((index>5)&&(index<55)) {
			if(index<10) {
				return "Severe Thinness ("+index+" kg/m2)\nMaybe Wrong Units";
			}else if((index>=10)&&(index<16)) {
				return "Severe Thinness ("+index+" kg/m2)";
			}else if((index>=16)&&(index<17)) {
				return "Moderate Thinness ("+index+" kg/m2)";
			}else if((index>=17)&&(index<18.5)) {
				return "Mild Thinness ("+index+" kg/m2)";
			}else if((index>=18.5)&&(index<25)) {
				return "Normal ("+index+" kg/m2)";
			}else if((index>=25)&&(index<30)) {
				return "Overweight ("+index+" kg/m2)";
			}else if((index>=30)&&(index<35)) {
				return "Obese Class I ("+index+" kg/m2)";
			}else if((index>=35)&&(index<40)) {
				return "Obese Class II ("+index+" kg/m2)";
			}else if((index>=40)&&(index<50)){
				return "Obese Class III ("+index+" kg/m2)";
			//at extreme results maybe the units are wrong
			}else {
				return "Obese Class III ("+index+" kg/m2)\nMaybe Wrong Units";
			}
		}else {
			return "Wrong Data";
		}
		
	}

}
